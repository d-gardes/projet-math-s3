# coding: utf-8

 ########################################################
 #                   NOTE IMPORTANTE                    #
 #  Pour des raisons d'obsolescence et de fin de vie    #
 #  imminente de Python 2 ce code à été réalisé avec    #
 #  Python 3. Et n'a donc pas été testé avec Python 2,  #
 #  il est donc possible que ce programme soit          #
 #  incompatible avec ce dernier.                       #
 ########################################################

import csv_parser as parser
import filtrage as filt
import prettyPrinter as pp
import time as time
import numpy as np
import os

# 2 méthodes

# nb notes perdues
# top n users

header = ["%perdu", "maxTopUser", "nbTotItems", "nbNotPredit", "margeErreurMoyenne", "EquartType", "erreurMoyenne%", "temp"]
results = []
tabComplet = parser.parseCsv('toy_complet.csv')

for pcrPerdu in range(5, 90, 5):
    print(pcrPerdu)
    beginTime = filt.currTime()
    tabIncomplet = parser.forgotXScores('toy_complet.csv', pcrPerdu)
    #tabIncomplet = parser.parseCsv('toy_incomplet.csv')
    tabPredictions = np.ndarray(shape=tabComplet.shape, dtype=int)

    maxTopUser = 10

    nbTotItems = 0  # Nb cases parcourues
    nbNotPredit = 0 # Nb notes prédites
    sommeEcart = 0  # Somme deltas erreur
    sumNeg = 0
    sumPos = 0
    biais = []

    for user in range(len(tabIncomplet)):
        for item in range(len(tabIncomplet[user])):
            nbTotItems = nbTotItems +1
            if filt.rating(tabIncomplet, user, item) == -1:
                predictRating = filt.approcheCentreeItem(tabIncomplet, user, item, maxTopUser)
                tabPredictions[user][item] = predictRating
                # tabIncomplet[user][item] = predictRating
                nbNotPredit = nbNotPredit +1
                ecartNote = (predictRating - filt.rating(tabComplet, user, item))
                if ecartNote < 0:
                    sumNeg+=1
                else:
                    sumPos+=1
                biais.append(ecartNote)
                sommeEcart += ecartNote
        pp.printProgressBar(user+1, len(tabIncomplet))

    # Ecart type :
    sommeEcartAuCarre = 0
    for biai in biais:
        sommeEcartAuCarre += (biai)**2
    ecart = np.sqrt(sommeEcartAuCarre)/nbNotPredit

    row = [pcrPerdu, maxTopUser, nbTotItems, nbNotPredit, round(sommeEcart/nbNotPredit, 3), ecart, round(((abs(sommeEcart)/nbNotPredit)/5) *100, 2), filt.currTime() - beginTime]

    results.append(row)

parser.writeCsv("pondération_varie_sur_pcrPerdu.csv", parser.dialectCsv("toy_complet.csv"), header, results)
