# coding: utf-8

import csv
import numpy as np
import sys
import random

# Parse a CSV into an array
def parseCsv(nameFile):
    data = [] # create list where we will store the csv data
    try:
        with open(nameFile, newline='') as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(1024)) # detect csv dialect
            csvfile.seek(0) # go to begin of file
            spamreader = csv.reader(csvfile, dialect)
            for row in spamreader:
                data.append(row[:100]) # add only 100 firts elements of the row
                #data.append(row) # add new row to list
        # csv file closed
    except FileNotFoundError as e:
        print("ERREUR: Impossible d'ouvrir le CSV : " + nameFile, file=sys.stderr)
        exit()

    # Now we convert strings to int
    for x in range(len(data)):
        for y in range(len(data[x])):
            data[x][y] = int(data[x][y])
        data[x] = np.array(data[x]) # Convert lists to np arrays
    data = np.array(data)

    return data

# Just return the dialect of a csv
def dialectCsv(nameFile):
    try:
        with open(nameFile, newline='') as csvfile:
            return csv.Sniffer().sniff(csvfile.read(1024)) # detect csv dialect
        # csv file closed
    except FileNotFoundError as e:
        print("ERREUR: Impossible d'ouvrir le CSV : " + nameFile, file=sys.stderr)
        exit()


def writeCsv(nameFile, dialect, header, data):
    try:
        with open("Super système super opti pour de super graphiques dans linux/" + nameFile, "w", newline='') as csvfile:
            csvfile.seek(0) # go to begin of file
            writer = csv.writer(csvfile, dialect)
            writer.writerow(header)
            for row in data:
                writer.writerow(row)

        # csv file closed
    except Exception as e:
        print("ERREUR: Impossible d'ouvrir le CSV : " + nameFile, file=sys.stderr)
        print("ERREUR: " + e, file=sys.stderr)
        exit()

# Forget a percentage of scores from nameFile
def forgotXScores(nameFile, percentage):

    # @params:
    #     nameFile    - Required  : Path to file (String)
    #     percentage  - Required  : Percentage of forgotten scores (Int)
    # @return: np.array()

    if percentage < 0 or percentage > 100:
        print("ERREUR: Un pourcentage est compris entre 0 et 100 et pas : " + str(percentage), file=sys.stderr)
        exit()
    else:
        data = parseCsv(nameFile)
        random.seed()
        for x in range(len(data)):
            for y in range(len(data[x])):
                if random.randrange(0, 100, step=1, _int=int) < int(percentage):
                    data[x][y] = -1
        return data
