# -*- coding: utf-8 -*-

 #######################################################
 #                   NOTE IMPORTANTE                   #
 #  Pour des raisons d'obsolescence et de fin de vie   #
 #  imminente de Python 2 ce code à été réalisé avec   #
 #  Python 3. Et n'a donc pas été testé avec Python 2  #
 #  Il est donc possible que ce programme soit         #
 #  incompatible avec ce dernier.                      #
 #######################################################

import numpy as np
import math as m
import time as time

# Ligne, colonne
# Numéro de ligne : utilisateur
# Numéro de colonne : numéro item
# Numéro ligne, colonne : note donnée par l'utilisateur i, pour l'item j


# shape retourne (nbr ligne, nbr colonne)

def info(tableau, user1, user2):
    print("User1 : %d, User2 : %d, Simil %s, Simil cos %s, ssq1 %s, ssq2 %s" % (user1,user2,similDefault(tableau, user1, user2),similCosine(tableau, user1, user2),sqrtSumSquaredRatingSubstract(tableau, user1, user2, averageRating(tableau, user1)),sqrtSumSquaredRatingSubstract(tableau, user2, user1, averageRating(tableau, user2))))
    print("NtMoy 1 %s, NtMoy2 %s" % (averageRating(tableau, user1), averageRating(tableau, user2)))
    list = listRatedItem(tableau, user1)
    sum = sumRating(tableau, user1)
    print("Somme1 %s, Taille 1 %s" % (sum, len(list)))
    list = listRatedItem(tableau, user2)
    sum = sumRating(tableau, user2)
    print("Somme1 %s, Taille 2 %s" % (sum, len(list)))

def currTime():
    # Renvoie le temps actuel de la machine
    return int(round(time.time() * 1000))

def rating(tableau, user, item, userMode = False):
    # Retourne la note donnée à un utilisateur sur un item
    # ou inversement si l'userMode est sur false
    # 'x' = L'utilisateur
    rating = 0
    if not userMode:
        rating = tableau[user][int(item)]
    else:
        rating = tableau[int(item)][user]
    return rating;

def sumRating(I, x, userMode = False):
    # Procéde à la somme des notes faites par un utilisateur
    # ou l'inverse si userMode est sur false
    # Ignore les notes négatives (donc non saisies)
    somme = 0
    for item in listRatedItem(I, x):
        somme += rating(I, x, item, userMode = userMode)
    return somme


def averageRating(I, x, userMode = False):
    # Renvoie la note moyenne attribué par un utilisateur x
    # ou l'inverse si userMode est sur false
    list = listRatedItem(I, x, userMode = userMode)
    return sumRating(I, x, userMode = userMode) / float(len(list))

def sqrtSumSquaredRating(I, x, userMode = False):
    # Renvoie la racine de la somme des notes au carré
    sum = 0;
    for i in listRatedItem(I, x, userMode = userMode):
        sum+=float(rating(I, x, i, userMode = userMode))**2
    return np.sqrt(sum)

def sqrtSumSquaredRatingSubstract(I, x, y, avgRating, userMode = False):
    # Renvoie la racine de la somme
    # des écarts a la moyenne au carré
    sum = 0;
    for i in listSameRatedItem(I, x, y, userMode = userMode):
        sum+=float((rating(I, x, i, userMode = userMode)-avgRating)**2)
    return np.sqrt(sum)

# Defini une similarité
# de comportement, type deux personnes mettent des notes extremes
# Du style x: 1, 1, 1, 1 / y: 5, 5, 5, 5
# La fonction renverra 1 du fait que les utilisateurs ont un comportement similaire
def similCosine(I, x, y, userMode = False):
    # Ixy
    sameItem = listSameRatedItem(I, x, y)
    sumSameItem = 0
    for i in sameItem:
#                           Rxi ²    *   Ryi ²
        sumSameItem+=(rating(I, x, i, userMode = userMode)*rating(I, y, i, userMode = userMode))
#
    sqrtItemX = sqrtSumSquaredRating(I, x, userMode = userMode)
    sqrtItemY = sqrtSumSquaredRating(I, y, userMode = userMode)
    simil = float(sumSameItem) / (sqrtItemX * sqrtItemY)
    return simil

# Défini une similarité
# au niveau des notations des utilisateurs
def similDefault(I, x, y, userMode = False):
    sameItem = listSameRatedItem(I, x, y, userMode = userMode)
    sumSameItem = 0.0;
    avgRatX = averageRating(I, x, userMode = userMode)
    avgRatY = averageRating(I, y, userMode = userMode)
    for i in sameItem:
        sumSameItem+=(rating(I, x, i, userMode = userMode)-avgRatX)*(rating(I, y, i, userMode = userMode) - avgRatY)
    sqrtItemX = sqrtSumSquaredRatingSubstract(I, x, y, avgRatX, userMode = userMode)
    sqrtItemY = sqrtSumSquaredRatingSubstract(I, y, x, avgRatY, userMode = userMode)
    simil = float(sumSameItem) / (sqrtItemX * sqrtItemY)
    return simil;

def listRatedItem(tableau, user, userMode = False):
    # Renvoie la liste des élèments ayant
    # été noté par un utilisateur x
    ratedItem = []
    for item in range(0, tableau.shape[1]):
        if rating(tableau, user, item, userMode = userMode) >= 0:
            ratedItem.append(item)
    return ratedItem

def listSameRatedItem(I, x, y, userMode = False):
    # Renvoie la liste des élèments ayant été noté
    # par l'utilisateur x et l'utilisateur y
    sameItem = []
    for i in range(0, I.shape[1]):
        if rating(I, x, i, userMode = userMode) >= 0 and rating(I, y, i, userMode = userMode) >= 0:
            sameItem.append(i)
    return sameItem

def mostSimilar(I, x, item, N, userMode = False):
    # Renvoie les utilisateurs les plus
    # similaires selon la fonction de similarité
    # qui utilise le cosinus
    simils = []
    for y in range(0, I.shape[0]):
        if y != x:
            if rating(I, y, item, userMode = userMode) >= 0:
                #similVal = similDefault(I, x, y, userMode = userMode)
                similVal = similCosine(I, x, y, userMode = userMode)
                simils.append([y, similVal])
    simils.sort(reverse = True, key=lambda x: x[1])
    simils = simils[:N]
    return simils

def listeItemRecommanded(tableauPrediction, user, N = 10):
    listItemRecommand = []
    for index in range(0, tableauPrediction.shape[1]):
        if(rating(tableauPrediction, user, index) >= 0):
            item = [index, rating(tableauPrediction, user, index)]
            listItemRecommand.append(item)
    listItemRecommand.sort(reverse = True, key=lambda x: x[1])
    listItemRecommand = listItemRecommand[:N]
    return listItemRecommand

def approcheCentreeItemNulle(tableau, user, item, N = 10):
    simils = mostSimilar(tableau, user, item, N);
    rNote = 0;
    for i in range(0, len(simils)):
        user_ = simils[i][0]
        noteUser = rating(tableau, user_, item)
        rNote += noteUser
    return 1 / N * float(rNote)

def approcheCentreeItem(I, x, item, N = 10):
    similar = mostSimilar(I, x, item, N)
    sommeEcartNote = 0.0
    sommeSimilaritéUser = 0.0
    for i in range(0, len(similar)):
        user = similar[i][0]
        valeur = similar[i][1]
                          # simil(u, u') * note u' sur i  -   moyenne note u'
        sommeEcartNote += valeur * (rating(I, user, item) - averageRating(I, user))
        sommeSimilaritéUser+=valeur
    if sommeSimilaritéUser == 0:
        sommeSimilaritéUser = 1
    quotient_K = 1 / sommeSimilaritéUser
    avg = averageRating(I, x)
         # moyenne u  +      k      *   somme simil(u,u') * ecart
    note = avg        +  (quotient_K*   sommeEcartNote)
    if note > 5 or note < 0:
        #print("Note wtf : note: %s, item: %s, user: %s" % (note, item, x))
        if note > 5:
            note = 5
        elif note < 0:
            note = 0
    return note
