# coding: utf-8

 ########################################################
 #                   NOTE IMPORTANTE                    #
 #  Pour des raisons d'obsolescence et de fin de vie    #
 #  imminente de Python 2 ce code à été réalisé avec    #
 #  Python 3. Et n'a donc pas été testé avec Python 2,  #
 #  il est donc possible que ce programme soit          #
 #  incompatible avec ce dernier.                       #
 ########################################################

import csv_parser as parser
import filtrage as filt
import prettyPrinter as pp
import time as time
import numpy as np
import os

pp.header()

tabComplet = parser.parseCsv('toy_complet.csv')
#tabIncomplet = parser.forgotXScores('toy_complet.csv', 90)
tabIncomplet = parser.parseCsv('toy_incomplet.csv')
tabPredictions = np.ndarray(shape=tabComplet.shape, dtype=float)

print("Fichiers ouverts avec succès\n")

print("Taille tab complet %d x %d" % (tabComplet.shape[0], tabComplet.shape[1]))

nbTotItems = 0  # Nb cases parcourues
nbNotPredit = 0 # Nb notes prédites
sommeEcart = 0  # Somme deltas erreur
sumNeg = 0
sumPos = 0
sumExact = 0

# print(nbperdu)
# print( str((nbperdu + 0.0)/nbTotItemsItems *100) + "% pertes")

maxTopUser = 10
biais = []

mArr = [[0, 1, 2], []]

for user in range(len(tabIncomplet)):
    for item in range(len(tabIncomplet[user])):
        nbTotItems = nbTotItems +1
        if filt.rating(tabIncomplet, user, item) == -1:
            predictRating = filt.approcheCentreeItem(tabIncomplet, user, item, maxTopUser)
            tabPredictions[user][item] = predictRating
            #tabIncomplet[user][item] = predictRating
            nbNotPredit = nbNotPredit +1
            ecartNote = predictRating - filt.rating(tabComplet, user, item)
            if ecartNote < 0:
                sumNeg+=1
            elif ecartNote == 0:
                sumExact+=1
            else:
                sumPos+=1
            biais.append(ecartNote)
            sommeEcart += ecartNote
    pp.printProgressBar(user+1, len(tabIncomplet))

# Ecart type :
sommeEcartAuCarre = 0
moyenneEcartNote = sum(biais)/len(biais)
for ecartNote in biais:
    sommeEcartAuCarre += (ecartNote - moyenneEcartNote)**2
ecart = np.sqrt(1/nbNotPredit*(sommeEcartAuCarre))

pp.footer(nbTotItems, nbNotPredit, ecart, sumNeg, sumPos, sumExact, sommeEcart)

filt.info(tabIncomplet, 0, 1)

userN = input("Veuillez saisir un numéro utilisateur:\n")
print("Voici les items recommandés pour l'utilisateur %s" % userN)
for couple in filt.listeItemRecommanded(tabPredictions, user = int(userN)):
    print("Item: %s, Note: %d, Note émulée: %f" % (couple[0], tabComplet[10][couple[0]], couple[1]))



#   python -m cProfile -s cumulative .\predictionDeNotes.py


# AVEC prise en compte des notes prédites : 250s   marge erreur 0.497    9.94%
# SANS prise en compte des notes prédites : 125s   marge erreur 0.502   10.04%
