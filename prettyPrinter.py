# coding: utf-8

# Print iterations progress
def printProgressBar (iteration, total, length = 51):

    # loop to create terminal progress bar
    # @params:
    #     iteration   - Required  : current iteration (Int)
    #     total       - Required  : total iterations (Int)
    #     length      - Optional  : character length of bar (Int)

    percent = ("{0:.1f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = '█' * filledLength + '-' * (length - filledLength)
    print('\r |%s| %s%% ' % (bar, percent), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

# Just print a string of 'fill' on 'length' characters
def space(length, fill = ' '):

    # @params:
    #     length    - Required  : length of space (Int)
    #     fill      - Optional  : Fill character (char)
    # @return: String

    return str(fill * int(length))

# Just print a pretty header
def header():
    print(space(60,'=') + "\n||" + space(56) + "||\n||     IUT INFORMATIQUE DE TOULOUSE - MODULE M3202        ||\n||   Apprentissage machine et systèmes de recommandation  ||\n||" + space(56)+"||\n||           Par Fabien Cayre et Gardes Dorian            ||\n||" + space(56)+"||\n" + space(60, '=') + '\n') # Ugly but static and final !

# Just print a pretty footer
def footer(nbTotItems, nbNotPredit, ecart, sumNeg, sumPos, sumExact, sommeEcart):

    print("\n\n" + space(60,'=') + "\n||")

    print("|| Sur " + str(nbTotItems) + " notes nous avons essayé d'en retrouver " + str(nbNotPredit))
    print("|| En moyenne nos prédictions ont une marge d'erreur de "+ str(round(sommeEcart/nbNotPredit, 3)))
    print("|| Ecart-Type des valeurs trouvées : %s" % ecart)
    print("|| Nombre de bias négatif : %s, nombre de biais positif : %s" % (sumNeg, sumPos))
    print("|| %s note ont été parfaitement trouvées sur %s" % (sumExact, nbNotPredit))
    print("|| Ce qui donne une erreur moyenne de "+ str(round(((abs(sommeEcart)/nbNotPredit)/5) *100, 2)) +"%")

    print("||\n" + space(60, '=') + '\n')
